package com.example.adam.incarphonereceiver;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

public class MainActivity extends AppCompatActivity {

    // The main button
    Button buttonCall, button;
    Button serviceStartButton;

    TextView info, infoip, msg;
    String message = "";
    ServerSocket serverSocket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Link the button
        buttonCall = (Button)findViewById(R.id.button);

        serviceStartButton = (Button)findViewById(R.id.serviceStart);

        info        = (TextView) findViewById(R.id.info);
        infoip      = (TextView) findViewById(R.id.infoip);
        msg         = (TextView) findViewById(R.id.msg);

        infoip.setText(getIpAddress());

//        Thread socketServerThread = new Thread(new SocketServerThread());
//        socketServerThread.start();


        findViewById(R.id.serviceStart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( v.getContext(), "START SERVICE", Toast.LENGTH_SHORT);
                Intent intent = new Intent(v.getContext(), CallReceiverService.class);
                startService(intent);
            }
        });

        setUpListener();
    }

    public void setUpListener(){

        final Button button = (Button) findViewById(R.id.button);
        final TextView tv = (TextView) findViewById(R.id.phoneNumber);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + tv.getText().toString()));
                startActivity(callIntent);
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {

                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();

                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();

                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "SiteLocalAddress: "
                                + inetAddress.getHostAddress() + "\n";
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }


}