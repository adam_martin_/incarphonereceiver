package com.example.adam.incarphonereceiver;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.TextView;

import com.example.adam.incarphonereceiver.MainActivity;
import com.example.adam.incarphonereceiver.R;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class SocketServerThread extends Thread {

    static final int SocketServerPORT = 8080;
    int count = 0;

    Context ctxt;

    public SocketServerThread(Context context) {
        // store parameter for later user
        this.ctxt = context;
    }

    @Override
    public void run() {

        Socket socket = null;
        DataInputStream dataInputStream = null;
        DataOutputStream dataOutputStream = null;

        try {
            ServerSocket serverSocket = new ServerSocket(SocketServerPORT);

            while (true) {
                socket = serverSocket.accept();
                dataInputStream = new DataInputStream(
                        socket.getInputStream());
                dataOutputStream = new DataOutputStream(
                        socket.getOutputStream());

                final String messageFromClient = dataInputStream.readUTF();

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                callIntent.setData(Uri.parse(messageFromClient));
                this.ctxt.startActivity(callIntent);

                Log.i("Button", "Call was pressed");


                String msgReply = "Hello from Android, you are #" + count;
                dataOutputStream.writeUTF(msgReply);

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            final String errMsg = e.toString();

        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dataOutputStream != null) {
                try {
                    dataOutputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
}